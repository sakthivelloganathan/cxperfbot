package cxPerfBot;

import com.deque.axe.AXE;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.core.har.*;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*Think RunId for accessibility and single user*/

public class cxPerfAnalyser extends driverSetup
{
    public String dbName = "BOTDB";
    public String dbUserName = "root";
    public String dbPassword = "root";
    public String rpName = "dRetentionPolicy";

    commonUtil commonUtilObj = new commonUtil();
    public String dbHost = this.commonUtilObj.getProperties("dbhost");
    public String nextRunID = getRunID();
    public InfluxDB influxDB = InfluxDBFactory.connect(this.dbHost, this.dbUserName, this.dbPassword);
    public String clientName = this.commonUtilObj.getProperties("clientName");
    public String projectName = this.commonUtilObj.getProperties("projectName");
    public boolean connected;
    /*   public Point.Builder point1 = null;
       public Point.Builder point2 = null;
      // public Point.Builder point3 = null;
       public Point.Builder point4 = null;
       public Point.Builder point5 = null;*/
    public String nxtRunID = this.nextRunID;

    /// Add DB error Handling

    /* private  void connect() {

        InfluxDB influxDB = InfluxDBFactory.connect(this.dbHost, this.dbUserName, this.dbPassword);
        // System.out.println(influxDB);

        // try {
             Pong response = influxDB.ping();
             System.out.println(response);
             if (!response.getVersion().equalsIgnoreCase("unknown")) {
                 connected = true;
                 System.out.println("Connected to database server InfluxDB");
             } else {
                 System.out.println("Database server InfluxDB not responding. Please check your configuration and/or connection");
                 connected = false;
             }
         *//*} catch (RuntimeException  e) {
            connected = false;
            influxDB.close();
        }*//*
    }
*/
    BatchPoints batchPoints = BatchPoints
            .database(dbName)
            .retentionPolicy(rpName)
            .consistency(InfluxDB.ConsistencyLevel.ALL)
            .build();

    public String getRunID() {

        InfluxDB influxDB = InfluxDBFactory.connect(this.dbHost, this.dbUserName, this.dbPassword);

        String returnID;

        if (!influxDB.databaseExists(this.dbName)) {
            influxDB.createDatabase(this.dbName);
            String dbQuery = "CREATE RETENTION POLICY dRetentionPolicy ON " + this.dbName + " DURATION 365d REPLICATION 1 DEFAULT";
            QueryResult queryResult = influxDB.query(new Query(dbQuery, this.dbName));

            influxDB.setDatabase(dbName);

            for (int i = 1; i <= 5; i++) {
                influxDB.write(Point.measurement("cxPerfBotResponeTime")
                        .tag("SLA", String.valueOf(i))
                        .field("SLA", i)
                        .build());
            }
            returnID = "1";

        } else {
            QueryResult queryResult = influxDB.query(new Query("SELECT LAST(runId) FROM cxPerfBotResponeTime", this.dbName));
            String id = queryResult.getResults().get(0).getSeries().get(0).getValues().get(0).get(1).toString();

            int nextRun = Integer.valueOf(id).intValue() + 1;

            returnID = Integer.toString(nextRun);
        }
        return returnID;
    }

    public void PerformanceRecomendationData(Har har, String pageTitle)//, String nxtRunID

    {
        List<Map<String, String>> myListOfMaps = new ArrayList<Map<String, String>>();
        Map<String, List<String>> myListOfMap = new HashMap<String, List<String>>();

        String path = System.getProperty("user.dir");

        File exe = new File(path + "\\dependency\\har_to_pagespeed.exe");
        File hhar = new File(path + "\\dependency\\temp.har");

        try {

            hhar.createNewFile();
            har.writeTo(hhar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> command = new ArrayList();

        command.add(exe.getPath());
        command.add(hhar.getPath());

        ProcessBuilder pb = new ProcessBuilder(command);

        Process p = null;
        try {

            p = pb.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            Pattern insightP = Pattern.compile("_(.*)_ \\(score=(0|[0-9][0-9]|100)\\)");
            Pattern descP = Pattern.compile("  (\\w+.*)");
            Pattern dataP = Pattern.compile("    \\* (\\w+.*)");

            String line;
            String strMitigation = "Mitigation~";
            int flag = 0;
            int i = 0;
            String keyVal = "";

            List<String> valSetOne = new ArrayList<String>();

            while ((line = reader.readLine()) != null) {
                Matcher m = insightP.matcher(line);
                if (m.matches()) {
                    if (flag == 1) {
                        myListOfMap.put(keyVal, valSetOne);
                        valSetOne = new ArrayList<String>();
                        strMitigation = "Mitigation~";
                        flag = 0;

                    }
                    if (flag == 0) {
                        int sc = Integer.parseInt(m.group(2));
                        String desc2 = (m.group(1));
                        keyVal = desc2 + "|" + sc;

//                        System.out.println("Fix at least :" + desc2);

                        flag = 1;
                    }
                }
                m = descP.matcher(line);
                if (m.matches()) {
                    String desc = (m.group(1));
                    valSetOne.add("Recommendation~" + desc);
                }
                m = dataP.matcher(line);
                if (m.matches()) {
                    String addDescData = (m.group(1));
                    valSetOne.add("Mitigation~" + addDescData);
                    strMitigation = strMitigation + ";" + addDescData;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Map.Entry<String, List<String>> entry : myListOfMap.entrySet()) {
            String key = entry.getKey();
            List<String> values = entry.getValue();
            String rulekey[] = key.split("|");

            String recom = "";
            for (int k = 0; k < values.size(); k++) {

                if (values.get(k).contains("Recommendation")) {
                    String str[] = values.get(k).split("~");
                    recom = str[1];
                }

                if (values.get(k).contains("Mitigation")) {
                    String str[] = values.get(k).split("~");
                    String miti = str[1];

                    Point point3= Point.measurement("cxPerfMitigation")
                            .tag("pageTitle", pageTitle)
                            .tag("projectName", projectName)
                            .tag("runId", nxtRunID)
                            .field("runId", nxtRunID)
                            .field("projectName", projectName)
                            .field("pageTitle", pageTitle)
                            .field("Rule", key)
                            .field("Recommendation", recom)
                            .field("Mitigation", miti)
                            .build();

                    influxDB.write(dbName,rpName,point3);
                    //         influxDB.write(batchPoints);
                }
            }

        }
        hhar.delete();
    }

    public void Responsetime(String pageTitle, double Responsetime, double RedirectTiming, double AppCacheTiming, double DomainLookupTime, double TCPConnectTime, double RequestTiming, double HTMLDownloadTime, double ProcessingTiming, double Onload, double NetworkLatency, double DOMReadyTime, double DOMBuildingTime, double FirstByteTime, double ResourceFetchTime, double frontEndTime, double backEndTime) //, Long firstPaint

    {
        Point point1 = Point.measurement("cxPerfBotResponeTime")
                .tag("projectName", this.projectName)
                .tag("pageTitle", pageTitle)
                .tag("runId", nxtRunID)
                .field("projectName", this.projectName)
                .field("runId", nxtRunID)
                .field("TransactionName", pageTitle)
                .field("ResponseTime", Double.valueOf(Responsetime))
                .field("RedirectTiming", Double.valueOf(RedirectTiming))
                .field("AppCacheTiming", Double.valueOf(AppCacheTiming))
                .field("DomainLookupTime", Double.valueOf(DomainLookupTime))
                .field("TCPConnectTime", Double.valueOf(TCPConnectTime))
                .field("RequestTiming", Double.valueOf(RequestTiming))
                .field("HTMLDownloadTime", Double.valueOf(HTMLDownloadTime))
                .field("ProcessingTiming", Double.valueOf(ProcessingTiming))
                .field("NetworkLatency", Double.valueOf(NetworkLatency))
                .field("DOMReadyTime", Double.valueOf(DOMReadyTime))
                .field("DOMBuildingTime", Double.valueOf(DOMBuildingTime))
                .field("FirstByteTime", Double.valueOf(FirstByteTime))
                .field("ResourceFetchTime", Double.valueOf(ResourceFetchTime))
                .field("frontEndTime", Double.valueOf(frontEndTime))
                .field("backEndTime", Double.valueOf(backEndTime))
                .build();

        influxDB.write(dbName,rpName,point1);

    }

    public void PerformanceData(Har har,String pageTitle)
    {
        PerformanceRecomendationData(har, pageTitle);//, nxtRunID

        HarLog harLog = har.getLog();

        List<HarEntry> entries = harLog.getEntries();
        for (HarEntry entry : entries)
        {
            HarResponse response = entry.getResponse();

            List<HarNameValuePair> responsess = response.getHeaders();

            HarContent content = response.getContent();

            String mime = content.getMimeType();
            String CEncoding = "";
            String CConnection = "";
            if (!responsess.isEmpty())
            {
                for (int l = 0; l < responsess.size(); l++)
                {
                    if (responsess.get(l).getName().equalsIgnoreCase("Content-Encoding"))
                    {
                        responsess.get(l).getValue();
                        CEncoding = responsess.get(l).getValue();
                    }
                    if (responsess.get(l).getName().equalsIgnoreCase("Connection"))
                    {
                        responsess.get(l).getValue();
                        CConnection = responsess.get(l).getValue();
                    }

                }

            }
            else
            {

            }

            if (mime.contains(";"))
            {
                String strsplit[] = mime.split(";");
                mime = strsplit[0];
                //System.out.println(mime);
            }
            String Text = content.getText();
            double Size = content.getSize();

            String PageRef = entry.getPageref();

            double PageLoadTime = entry.getTime();

            PageLoadTime = PageLoadTime / 1000;

            HarTimings timings = entry.getTimings();

            double blocked = timings.getBlocked();
            double connect = timings.getConnect();
            double dns = timings.getDns();
            double recieve = timings.getReceive();
            double send = timings.getSend();
            double ssl = timings.getSsl();
            double wait = timings.getWait();

            blocked = blocked / 1000;
            connect = connect / 1000;
            dns = dns / 1000;
            recieve = recieve / 1000;
            send = send / 1000;
            ssl = ssl / 1000;
            wait = wait / 1000;

            String connection = entry.getConnection();

            HarRequest request = entry.getRequest();

            double requestBodySize = request.getBodySize();
            double requestHeadersSize = request.getHeadersSize();

            String requestURL = request.getUrl();

            String fileName = requestURL.substring(requestURL.lastIndexOf("/"));

            double responseBodySize = response.getBodySize();
            double responseHeadersSize = response.getHeadersSize();

            int status = response.getStatus();

            double totalSize = responseBodySize + responseHeadersSize;

            totalSize = totalSize / 1024;

            Point point2 = Point.measurement("cxPerfBotTxnName")
                    .tag("pageTitle", pageTitle)
                    .tag("projectName", projectName)
                    .tag("Url", fileName)
                    .tag("runId", nxtRunID)
                    .field("runId", nxtRunID)
                    .tag("content", mime)
                    .field("projectName", projectName)
                    .field("pageTitle", pageTitle)
                    .field("CEncoding", CEncoding)
                    .field("CConnection", CConnection)
                    .field("Url", fileName)
                    .field("content", mime)
                    .field("dns", Double.valueOf(dns))
                    .field("connect", Double.valueOf(connect))
                    .field("send", Double.valueOf(send))
                    .field("ssl", Double.valueOf(ssl))
                    .field("wait", Double.valueOf(wait))
                    .field("Blocked", Double.valueOf(blocked))
                    .field("recieve", Double.valueOf(recieve))
                    .field("totalSize", Double.valueOf(totalSize))
                    .field("totalSize", Double.valueOf(totalSize))
                    .field("PageLoadTime", Double.valueOf(PageLoadTime))
                    .build();

            influxDB.write(dbName,rpName,point2);

        }
        // influxDB.write(batchPoints);
    }

    public void AccessibilityData(JSONArray violations,JSONArray passes,String pageTitle)

    {
        String Issuedescription = null;
        String ElementSource = null;
        String Rule = null;
        String passesElementSource = null;
        String message = null;
        JSONObject passesobj;
        JSONObject obj;

        List<String> obj7 = new ArrayList<String>();
        List<String> obj6 = new ArrayList<String>();
        List<String> obj8 = new ArrayList<String>();

        for (int ip = 0; ip < passes.length(); ip++)
        {
            passesobj = passes.getJSONObject(ip);
            JSONArray passesnodes = (JSONArray) passesobj.get("nodes");

            for (int jp = 1; jp <= passesnodes.length(); jp++)
            {

                JSONObject obj2 = passesnodes.getJSONObject(jp - 1);
                passesElementSource = (String) obj2.get("html").toString();

                Point point4= Point.measurement("cxAccessibilitypass")
                        .tag("pageTitle", pageTitle)
                        .tag("projectName", projectName)
                        .tag("runId", nxtRunID)
                        .field("runId", nxtRunID)
                        .field("projectName", projectName)
                        .field("pageTitle", pageTitle)
                        .field("Pass", passesElementSource)
                        .build();

                influxDB.write(dbName,rpName,point4);
            }
        }
        for (int i = 0; i < violations.length(); i++)
        {
            obj = violations.getJSONObject(i);

            Issuedescription = (String) obj.get("help");
            Rule = (String) obj.get("description");

            JSONArray violationsnodes = (JSONArray) obj.get("nodes");

            for (int j = 1; j <= violationsnodes.length(); j++)
            {
                JSONObject obj2 = violationsnodes.getJSONObject(j - 1);
                ElementSource = (String) obj2.get("html");
                JSONArray nodesnone = (JSONArray) obj2.get("none");

                for (int l = 1; l <= nodesnone.length(); l++)
                {
                    JSONObject obj22 = (JSONObject) nodesnone.get(l - 1);
                    message = (String) obj22.get("message");
//                    System.out.println("Fix at least :" + message);
                    if (message != null)
                    {
                        obj7.add(message);
                    }
                }
                JSONArray nodes11 = (JSONArray) obj2.get("any");
                for (int k = 1; k <= nodes11.length(); k++)
                {
                    JSONObject obj22 = (JSONObject) nodes11.get(k - 1);
                    message = (String) obj22.get("message");
//                                        System.out.println("Fix at least two :" + message);
                    if (message != null)
                    {
                        obj7.add(message);
                    }
                }
                Map<String, Object> object = new HashMap<String, Object>();
                object.put("message", obj7);
                Object Recommedation = object.get("message").toString();

                Point point5= Point.measurement("cxAccessibilityfail")
                        .tag("pageTitle", pageTitle)
                        .tag("projectName", projectName)
                        .tag("runId", nxtRunID)
                        .field("runId", nxtRunID)
                        .field("projectName", projectName)
                        .field("pageTitle", pageTitle)
                        .field("message", Recommedation)
                        .field("Issuedescription", Issuedescription)
                        .field("Fail", ElementSource)
                        .build();

                influxDB.write(dbName,rpName,point5);
            }
        }
    }

    public long StartTransction() {

        long startTime = new Date().getTime();
        return startTime;
    }

    public void EndTransction(String pageTitle, long startTime) {

        Har har = server.getHar();

        System.out.println("Entries count :"+  har.getLog().getEntries().size());

        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();

        Map<String, Object> timings = new HashMap();

        DecimalFormat df = new DecimalFormat("0.0000");

        double endTime = new Date().getTime();

        double Responsetime = Double.valueOf(Double.parseDouble(df.format((endTime - startTime))) / 1000d);

        JavascriptExecutor js = (JavascriptExecutor) driver;

        if (browserName.equals("Chrome") || browserName.equals("firefox") || browserName.equals("geckodriver") || browserName.equals("phantomjs")) {
            timings = (Map) js.executeScript("var performance = window.performance || window.webkitPerformance || window.mozPerformance || window.msPerformance || {};var timings = performance.timing || {}; return timings;", new Object[0].toString());

        } else {
            String jsonString = (String) js.executeScript("return JSON.stringify(window.performance.timing);");
            JSONObject json = new JSONObject(jsonString);
            timings = commonUtil.toMap(json);
        }
        // long unloadEventEnd = (Long) timings.get("unloadEventEnd");
        long responseEnd = (Long) timings.get("responseEnd");
        long responseStart = (Long) timings.get("responseStart");
        //long domInteractive = (Long) timings.get("domInteractive");
        long domainLookupEnd = (Long) timings.get("domainLookupEnd");
        //long unloadEventStart = (Long) timings.get("unloadEventStart");
        long domComplete = (Long) timings.get("domComplete");
        long domContentLoadedEventStart = (Long) timings.get("domContentLoadedEventStart");
        long domainLookupStart = (Long) timings.get("domainLookupStart");
        //long redirectEnd = (Long) timings.get("redirectEnd");
        //long redirectStart = (Long) timings.get("redirectStart");
        long connectEnd = (Long) timings.get("connectEnd");
        long connectStart = (Long) timings.get("connectStart");
        long loadEventStart = (Long) timings.get("loadEventStart");
        long navigationStart = (Long) timings.get("navigationStart");
        //long requestStart = (Long) timings.get("requestStart");
        //long secureConnectionStart = (Long) timings.get("secureConnectionStart");
        long fetchStart = (Long) timings.get("fetchStart");
        //long domContentLoadedEventEnd = (Long) timings.get("domContentLoadedEventEnd");
        long domLoading = (Long) timings.get("domLoading");
        long loadEventEnd = (Long) timings.get("loadEventEnd");

        double RedirectTiming = Double.valueOf(Double.parseDouble(df.format((fetchStart - navigationStart))) / 1000d);
        double AppCacheTiming = Double.valueOf(Double.parseDouble(df.format((domainLookupStart - fetchStart))) / 1000d);
        double DomainLookupTime = Double.valueOf(Double.parseDouble(df.format((domainLookupEnd - domainLookupStart))) / 1000d);
        double TCPConnectTime = Double.valueOf(Double.parseDouble(df.format((connectEnd - connectStart))) / 1000d);
        double TTFB = Double.valueOf(Double.parseDouble(df.format((responseStart - connectEnd))) / 1000d);
        double HTMLDownloadTime = Double.valueOf(Double.parseDouble(df.format((responseEnd - responseStart))) / 1000d);
        double ProcessingTiming = Double.valueOf(Double.parseDouble(df.format((loadEventStart - domLoading))) / 1000d);
        double Onload = Double.valueOf(Double.parseDouble(df.format((loadEventEnd - loadEventStart))) / 1000d);
        double TotalPageLoadTime = Double.valueOf(Double.parseDouble(df.format((loadEventEnd - navigationStart))) / 1000d);
        double NetworkLatency = Double.valueOf(Double.parseDouble(df.format((responseEnd - fetchStart))) / 1000d);
        double DOMReadyTime = Double.valueOf(Double.parseDouble(df.format((domContentLoadedEventStart - navigationStart))) / 1000d);
        double DOMBuildingTime = Double.valueOf(Double.parseDouble(df.format((domContentLoadedEventStart - responseEnd))) / 1000d);
        double renderTime = Double.valueOf(Double.parseDouble(df.format((domComplete - domLoading))) / 1000d);
        double FirstByteTime = Double.valueOf(Double.parseDouble(df.format((responseStart - navigationStart))) / 1000d);
        double ResourceFetchTime = Double.valueOf(Double.parseDouble(df.format((loadEventEnd - domContentLoadedEventStart))) / 1000d);

        double frontEndTime = Double.valueOf(Double.parseDouble(df.format((loadEventStart - responseStart))) / 1000d);
        double backEndTime = Double.valueOf(Double.parseDouble(df.format((responseStart - navigationStart))) / 1000d);

        Responsetime(pageTitle, Responsetime, RedirectTiming, AppCacheTiming, DomainLookupTime, TCPConnectTime, TTFB, HTMLDownloadTime, ProcessingTiming, Onload, NetworkLatency, DOMReadyTime, DOMBuildingTime, FirstByteTime, ResourceFetchTime, frontEndTime, backEndTime);//firstPaint);

        PerformanceData(har,pageTitle);

        String path = System.getProperty("user.dir");

        File harFile = new File(path + "\\dependency\\IE.har");

        try {
            harFile.createNewFile();
            har.writeTo(harFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // public JSONArray runaxe(WebDriver driver, URL scriptUrl) throws JSONException {
    public void runaxe(String pageTitle, URL scriptUrl)
    {
        JSONArray violations = null;
        JSONArray passes = null;

        System.out.println(driver);

        JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).analyze();

        violations = responseJSON.getJSONArray("violations");

        passes = responseJSON.getJSONArray("passes");

        AccessibilityData(violations, passes, pageTitle);

    }

    public void proxyStop()
    {
        if (driver != null)
        {
            server.stop();
            driver.quit();
        }
    }
}
