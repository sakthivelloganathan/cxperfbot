package cxPerfBot;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.mitm.manager.ImpersonatingMitmManager;
import net.lightbody.bmp.proxy.CaptureType;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

public class driverSetup {

    public WebDriver driver;
    public BrowserMobProxyServer server;
    commonUtil commonUtilObj = new commonUtil();

    public String Latency = commonUtilObj.getProperties("latency");
    public String downloadBandwidthLimit = commonUtilObj.getProperties("downloadBandwidthLimit");
    public String uploadBandwidthLimit = commonUtilObj.getProperties("uploadBandwidthLimit");
//    public String hostIp = commonUtilObj.getProperties("hostname");
    public String port = commonUtilObj.getProperties("port");
    public String login = commonUtilObj.getProperties("login");

    public Proxy getSeleniumProxy(BrowserMobProxy proxyServer) {
        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
        try {

        String hostIp = Inet4Address.getLocalHost().getHostAddress();
        System.out.println(hostIp);
//        System.out.println("setsslProxy"+seleniumProxy.setSslProxy(hostIp));
            System.out.println("setHttpProxy"+seleniumProxy.setHttpProxy(hostIp + ":" + proxyServer.getPort()));
            System.out.println("setsslProxy"+seleniumProxy.setSslProxy(hostIp + ":" + proxyServer.getPort()));

        } catch (UnknownHostException e)
        {
            e.printStackTrace();
            System.out.println("Invalid Host Address");
        }
        return seleniumProxy;
    }

    public WebDriver setupDriverCapabilities(String browserType, String driverPath) {
        server = new BrowserMobProxyServer();
        int port = 8080;
        server.setTrustAllServers(true);
//8080
        server.start();//server.getPort(), getSeleniumProxy(server)

        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(server);

        server.enableHarCaptureTypes(CaptureType.getRequestCaptureTypes());
        server.enableHarCaptureTypes(CaptureType.getResponseCaptureTypes());

        System.out.println("BrowserMob Proxy running on port: " + server.getPort());

       // Proxy seleniumProxy = getSeleniumProxy(server);

        System.out.println("Selenium: "+seleniumProxy);

        DesiredCapabilities seleniumCapabilities = new DesiredCapabilities();
        seleniumCapabilities.setCapability(CapabilityType.PROXY, seleniumProxy);

        if(Long.valueOf(Latency)>0 || Long.valueOf(downloadBandwidthLimit)>0 || Long.valueOf(uploadBandwidthLimit)>0)
        {
            server.setLatency(Long.valueOf(Latency), TimeUnit.MILLISECONDS);
            server.setReadBandwidthLimit(Long.valueOf(downloadBandwidthLimit));
            server.setWriteBandwidthLimit(Long.valueOf(uploadBandwidthLimit));
        }

        server.newHar();

        if (browserType.equalsIgnoreCase("IE"))
        {
            seleniumCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            seleniumCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            seleniumCapabilities.setCapability("ignoreZoomSetting", true);
            seleniumCapabilities.setCapability("ie.ensureCleanSession", true);
            seleniumCapabilities.setCapability("ie.setProxyByServer", true);
            seleniumCapabilities.setCapability("ie.usePerProcessProxy", false);
            seleniumCapabilities.setCapability("nativeEvents", true);

            System.setProperty("webdriver.ie.driver", driverPath);
            driver = new InternetExplorerDriver(seleniumCapabilities);
        }
        else if (browserType.equalsIgnoreCase("chrome"))
        {
            System.setProperty("webdriver.chrome.driver", driverPath);

            ChromeOptions options = new ChromeOptions();
//            options.addArguments("--ignore-certificate-errors");

            seleniumCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            seleniumCapabilities.setCapability(ChromeOptions.CAPABILITY, options);

            driver = new ChromeDriver(seleniumCapabilities);
        }
        else if (browserType.equalsIgnoreCase("HeadlessChrome"))
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("headless");
            chromeOptions.addArguments("window-size=1200x600");
            driver = new ChromeDriver(seleniumCapabilities);

        }
        else if (browserType.equalsIgnoreCase("phantomjs"))
        {
            String[] cli_args = new String[]{ "--ssl-protocol=any", "--ignore-ssl-errors=true", "--web-security=false" };
            seleniumCapabilities.setJavascriptEnabled(true);
            seleniumCapabilities.setCapability( PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cli_args );
            seleniumCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, driverPath);
            driver = new PhantomJSDriver(seleniumCapabilities);
        }
        else if (browserType.equalsIgnoreCase("firefox"))
        {
            System.setProperty("webdriver.gecko.driver", driverPath);
            driver = new FirefoxDriver(seleniumCapabilities);
        }

        else {
            System.out.println("No Drivers Selected");
        }
        return driver;
    }
}