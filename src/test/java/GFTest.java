import com.deque.axe.AXE;
import cxPerfBot.cxPerfAnalyser;
//import org.junit.Test;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

public class GFTest {

    @Test

    public void launchBrowser() throws MalformedURLException {
        cxPerfAnalyser cxPerfAnalyserObj = new cxPerfAnalyser();
        long strattime;

        String browserType = "chrome";

        String driverPath = "C:\\Users\\patricia\\Downloads\\chromedriver_win32\\chromedriver.exe";
//      String driverPath = "C:\\Users\\patricia\\IdeaProjects\\cxPerfBot\\src\\main\\resources\\drivers\\IEDriverServer.exe";
        //   String driverPath = "C:\\Users\\patricia\\IdeaProjects\\cxPerfBot\\src\\main\\resources\\drivers\\phantomjs.exe";
//     String driverPath = "C:\\Users\\patricia\\IdeaProjects\\cxPerfBot\\src\\main\\resources\\drivers\\geckodriver.exe";

        WebDriver wbDriver = null;

            wbDriver = cxPerfAnalyserObj.setupDriverCapabilities(browserType, driverPath);


        strattime = cxPerfAnalyserObj.StartTransction();

        URL scriptUrl = GFTest.class.getResource("axe.min.js");

        System.out.println(scriptUrl);

        wbDriver.get("https://www.theaa.com/");

        cxPerfAnalyserObj.EndTransction("S01_T01_theaa.com", strattime);

        cxPerfAnalyserObj.runaxe("S01_T01_theaa.com", scriptUrl);

        cxPerfAnalyserObj.proxyStop();

    }

}